# SY10

SY10 Évaluation d'un Site Web

**Un site Internet de qualité, c'est quoi ?**
 
Créer un site Internet de **qualité** nécessite de réunir les différentes conditions suivantes visant finalement à **satisfaire les cibles** pour lesquelles le site Internet est conçu.

1. L'Utilité du site web :
    C'est le seul critère qui dépend purement du maitre d'ouvrage.
    Pour être efficace un site Internet doit être utile ! Il ne suffit pas de disposer d'un très bon outil pour générer un chiffre d'affaires !
                
2. La visibilité du site :
    Pour être efficace un site Internet doit être visible c'est à dire accessible au maximum d'internautes ne le connaissant pas à priori. Il est donc nécessaire d'acquérir de la visibilité sur les moteurs de recherche, les réseaux sociaux et par tous les canaux offerts par le marketing et le webmarketing ! 

3. L'actualisation du site :
    Un site Internet qui n'évolue pas fréquemment ne peut pas séduire les internautes et est plutôt mal jugé par les moteurs de recherche qui ont tendance à le dévaloriser. 
            
4. La séduction du site :  
    Le potentiel de séduction, c'est à dire l'aspect graphique d'un site est un point important dans les processus de conquête et de fidélisation des internautes. Le site doit pouvoir se distinguer de ses concurrents, apporter une image moderne et novatrice en phase avec son positionnement. 
                
5. La fiabilité :
    C'est un point majeur de la réussite d'un site Internet, simplement parce que les outils de gestion et d'administration de sites Internet sont souvent connus pour dysfonctionner tant au niveau de l'internaute qui le consulte qu'au niveau de l'administrateur qui le gère. 

6. L'accessibilité du site (auditif, cognitif, neurologique, physique, de la parole, visuel):

    L'accessibilité caractérise la capacité pour un site Internet à être accessible à tous quelque soient les capacités physiques, psychiques ou éventuels handicaps.
    L’accessibilité du web ne bénéficie pas qu’aux personnes en situation de handicap. Elle prend aussi en compte :
    – les seniors dont les capacités changent avec l’âge
    – les individus ayant un « handicap temporaire » tel qu’un bras cassé ou perdu leurs lunettes
    – les personnes ayant « une limitation situationnelle » comme être en plein soleil ou dans un environnement où elles ne peuvent pas écouter l’audio
    – les personnes utilisant une connexion internet lente ou ayant une bande passante limitée ou onéreuse.

    C'est simplement la capacité qu'aura le site à être pratique, fonctionnel quelques soient les capacités de l'internaute, son équipement informatique, son mode de consultation. Proposer un site utilisable nécessite de concevoir le site web par l'approche UX Design (conception orientée utilisateur). 
