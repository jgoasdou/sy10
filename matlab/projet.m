%% Variables paramètre

 if exist('savearg','var') == 0
     savearg = 1;
 end
%% Initialisation des Tableaux
irr1 = [];
irr2 = [];
irr3 = [];
irr4 = [];
irr5 = [];
irr6 = [];
irr7 = [];
irr8 = [];
irr9 = [];
irr10 = [];
irr11 = [];
irr12 = [];
irr13 = [];

%% Initialisation des Systèmes flous
SF1 = readfis('SF1.fis');
SF2 = readfis('SF2.fis');
SF3 = readfis('SF3.fis');
SF4 = readfis('SF4.fis');
SF5 = readfis('SF5.fis');
SF6 = readfis('SF6.fis');
SF7 = readfis('SF7.fis');
SF8 = readfis('SF8.fis');
SF9 = readfis('SF9.fis');
SF10 = readfis('SF10.fis');
SF11 = readfis('SF11.fis');
SF12 = readfis('SF12.fis');
SF13 = readfis('SF13.fis');

%% Initialisation des intitulés du questionnaire
prompt1={...
    'SF1 : Langues {0 ; 1 ; 2} :',... 
    'SF1 : WCAG {0 ; 1 ; 2} :',...
    'SF2 : Adaptation_mobile [0;1] :',...
    'SF2 : Niveau de Performance [0;100] :',...
    "SF9 : Frequence d'actualisation du site [0;100] :",...
    "SF9 : Frequence d'actualisation des données [0;100] :",...
    "SF3 : Fiabilité du contenu de la page (virus) [0;100] :",...
    "SF3 : Protocol HTTPS {0;1} :",...
    "SF4 : Politique du site {0} U ]0;100] :",...
    "SF4 : Information sur le proprietaire du domaine {0} U ]0;100] :",...
    };
   prompt2={...
    'SF5 : Formes [0;1] :'...
    'SF5 : Architecture Flat [0;1] : '...
    'SF5 : Police [0;1] : '...
    'SF5 : Couleur [0;1] :'...
    'SF11 : Cohérence [0;1]: '...
    'SF6 : Architecture Usable [0;100]:'...
    "SF6 : Nombre d'actions pour atteindre l'information [10;1]"...
    'SF7 : Quoi Faire [0;1] Intuitif '...
    'SF7 : Comment Faire [0;1] Intuitif '...
    'SF7 : Quand Faire [0;1] Intuitif '...
    'SF13 : Référencement [30;1] :'...
    'SF13 : Fréquentation [1000000;0] :'...    
    };

%% Valeurs préenregistrées
prompt = {"0 Manuel ; 1 Zara ; 2 Poulpe"};
def = {'0'};
dlgtitle = "Évaluation d'un site Web"; 
lineNo = 1;
choix = inputdlg(prompt, dlgtitle, lineNo, def);

Zara1 = {'2','1','1','100', '90', '95', '100', '1', '85', '80'};
Zara2 = {'0.90', '0.70','0.85', '0.95', '0.90', '55','3','0.9','0.65','0.75','1', '500'};

Poulpe1 = {'0','0','1','80', '15', '50', '100', '1', '25', '0'};
Poulpe2 = {'1', '1','0.5', '0.75', '0.75', '50','2','1','0.7','0.6','7', '1000000'};


%% On définit des valeurs par défaut
if choix{1} == '0'
    if savearg == 0 || (exist('answer1','var') == 0)||(exist('answer2','var') == 0)|| isempty(answer1) || isempty(answer2)
        def1 = {'0','1','0.7','60', '0', '0', '0', '0', '0', '0'};
        def2 = {'1', '0','0', '0', '0', '0','1','0','0','0','1', '0'};
    else
        def1 = answer1;
        def2 = answer2;
    end
elseif choix{1} == '1'
    def1 = Zara1;
    def2 = Zara2;
elseif choix{1} == '2' 
    def1 = Poulpe1;
    def2 = Poulpe2;
end

answer1 = inputdlg(prompt1, dlgtitle, lineNo, def1); %Exécution du questionnaire
answer2 = inputdlg(prompt2, dlgtitle, lineNo, def2); %Exécution du questionnaire



%% Récupération des données entrées dans le questionnaire

Langues= str2num(answer1{1});
WCAG= str2num(answer1{2});
Adaptation_mobile= str2num(answer1{3});
Performances= str2num(answer1{4});
ActualisationSite= str2num(answer1{5});
ActualisationData= str2num(answer1{6});
FiabiliteVirus= str2num(answer1{7});
Protocol= str2num(answer1{8});
PolitiqueSite= str2num(answer1{9});
ProprietaireDomaine= str2num(answer1{10});
Formes= str2num(answer2{1});
ArchitectureFlat= str2num(answer2{2});
Police= str2num(answer2{3});
Couleur= str2num(answer2{4});
Coherence= str2num(answer2{5});
ArchitectureUsable= str2num(answer2{6});
NbrAction= str2num(answer2{7});
QuoiF= str2num(answer2{8});
CommentF= str2num(answer2{9});
QuandF= str2num(answer2{10});
Referencement= str2num(answer2{11});
Frequentation= str2num(answer2{12});


%% sf1 UTILISATION DE EVAL FIS

[sortie,irr1,orr,arr]= evalfis(SF1,[WCAG, Langues]);
%% calcul declenchement de chaque regle du SF1

declenchementSF1 = min(irr1,[],2);

%%conséquence finale du SF1 
%Initialisation de la conséquence finale 
nbruleSF1=length(SF1.rule); %nombre de règles
nbCsqSF1=length(SF1.output.mf); %Nombres de classes de sortie
csqSF1=zeros(1,nbCsqSF1);
for i = 1:nbruleSF1,
    csqSF1(SF1.rule(i).consequent)=max(csqSF1(SF1.rule(i).consequent), declenchementSF1(i));
    
end;

%Affichage de la conséquence SF1
%Concaténation de texte 
CsqSF1Txt ='';
for i = 1:nbCsqSF1
    CsqSF1Txt=[CsqSF1Txt, sprintf('( %s|%.2f)',SF1.output.mf(i).name, csqSF1(i))];
end
CsqSF1Txt= sprintf('SF1 %s : { %s }', SF1.output.name,CsqSF1Txt(1:end));
disp(CsqSF1Txt);

%% sf2 UTILISATION DE EVAL FIS

[sortie,irr2,orr,arr]= evalfis(SF2,[Performances, Adaptation_mobile]);
%% calcul declnchement de chaque regle du SF2

declenchementSF2=min(irr2,[],2);

%%conséquence finale du SF2 
%Initialisation de la conséquence finale 
nbruleSF2=length(SF2.rule); %nombre de règles
nbCsqSF2=length(SF2.output.mf); %Nombres de classes de sortie
csqSF2=zeros(1,nbCsqSF2);
for i = 1:nbruleSF2
    csqSF2(SF2.rule(i).consequent)=max(csqSF2(SF2.rule(i).consequent),declenchementSF2(i));
    
end;

%Affichage de la conséquence SF2
%Concaténation de texte 
CsqSF2Txt ='';
for i = 1:nbCsqSF2
    CsqSF2Txt=[CsqSF2Txt, sprintf('( %s|%.2f)',SF2.output.mf(i).name, csqSF2(i))];
end
CsqSF2Txt= sprintf('SF2 %s : { %s }', SF2.output.name,CsqSF2Txt(1:end));
disp(CsqSF2Txt);

%% SF8 
%Initialisation de la conséquence finale 
nbruleSF8=length(SF8.rule); %nombre de règles
nbCsqSF8=length(SF8.output.mf); %Nombres de classes de sortie

for i = 1:nbruleSF8
    irr8(i,1) = csqSF2(SF8.rule(i).antecedent(1));
    irr8(i,2) = csqSF1(SF8.rule(i).antecedent(2));
end

declenchementSF8 = min(irr8, [], 2);

csqSF8 = zeros(1, nbCsqSF8);
for i = 1:nbruleSF8
    csqSF8(SF8.rule(i).consequent)=max(csqSF8(SF8.rule(i).consequent),declenchementSF8(i));  
end

CsqSF8Txt ='';
for i = 1:nbCsqSF8
    CsqSF8Txt=[CsqSF8Txt, sprintf('( %s|%.2f)',SF8.output.mf(i).name, csqSF8(i))];
end
CsqSF8Txt= sprintf('SF8 %s : { %s }', SF8.output.name,CsqSF8Txt(1:end));
disp(CsqSF8Txt);

%% sf9 UTILISATION DE EVAL FIS

[sortie,irr9,orr,arr]= evalfis(SF9,[ActualisationData, ActualisationSite]);

%% calcul declenchement de chaque regle du SF9

declenchementSF9 = min(irr9,[],2);

%%conséquence finale du SF9 
%Initialisation de la conséquence finale 
nbruleSF9=length(SF9.rule); %nombre de règles
nbCsqSF9=length(SF9.output.mf); %Nombres de classes de sortie
csqSF9=zeros(1,nbCsqSF9);
for i = 1:nbruleSF9,
    csqSF9(SF9.rule(i).consequent)=max(csqSF9(SF9.rule(i).consequent), declenchementSF9(i));
    
end;


%Affichage de la conséquence SF9
%Concaténation de texte 
CsqSF9Txt ='';
for i = 1:nbCsqSF9
    CsqSF9Txt=[CsqSF9Txt, sprintf('( %s|%.2f)',SF9.output.mf(i).name, csqSF9(i))];
end
CsqSF9Txt= sprintf('SF9 %s : { %s }', SF9.output.name,CsqSF9Txt(1:end));
disp(CsqSF9Txt);


%% sf3 UTILISATION DE EVAL FIS

[sortie3,irr3,orr3,arr3]= evalfis(SF3,[Protocol, FiabiliteVirus]);

%% calcul declenchement de chaque regle du SF3

declenchementSF3 = min(irr3,[],2);

%%conséquence finale du SF3 
%Initialisation de la conséquence finale 
nbruleSF3=length(SF3.rule); %nombre de règles
nbCsqSF3=length(SF3.output.mf); %Nombres de classes de sortie
csqSF3=zeros(1,nbCsqSF3);
for i = 1:nbruleSF3
    csqSF3(SF3.rule(i).consequent)=max(csqSF3(SF3.rule(i).consequent), declenchementSF3(i));
    
end


%Affichage de la conséquence SF3
%Concaténation de texte 
CsqSF3Txt ='';
for i = 1:nbCsqSF3
    CsqSF3Txt=[CsqSF3Txt, sprintf('( %s|%.2f)',SF3.output.mf(i).name, csqSF3(i))];
end
CsqSF3Txt= sprintf('SF3 %s : { %s }', SF3.output.name,CsqSF3Txt(1:end));
disp(CsqSF3Txt);


%% sf4 UTILISATION DE EVAL FIS

[sortie,irr4,orr,arr]= evalfis(SF4,[PolitiqueSite, ProprietaireDomaine]);

%% calcul declenchement de chaque regle du SF4

declenchementSF4 = min(irr4,[],2);

%%conséquence finale du SF4 
%Initialisation de la conséquence finale 
nbruleSF4=length(SF4.rule); %nombre de règles
nbCsqSF4=length(SF4.output.mf); %Nombres de classes de sortie
csqSF4=zeros(1,nbCsqSF4);
for i = 1:nbruleSF4
    csqSF4(SF4.rule(i).consequent)=max(csqSF4(SF4.rule(i).consequent), declenchementSF4(i));
    
end


%Affichage de la conséquence SF4
%Concaténation de texte 
CsqSF4Txt ='';
for i = 1:nbCsqSF4
    CsqSF4Txt=[CsqSF4Txt, sprintf('( %s|%.2f)',SF4.output.mf(i).name, csqSF4(i))];
end
CsqSF4Txt= sprintf('SF4 %s : { %s }', SF4.output.name,CsqSF4Txt(1:end));
disp(CsqSF4Txt);

%% sf5 UTILISATION DE EVAL FIS

[sortie,irr5,orr,arr]= evalfis(SF5,[Formes, ArchitectureFlat, Police, Couleur]);
%% calcul declnchement de chaque regle du SF5

declenchementSF5 = min(irr5,[],2);

%%conséquence finale du SF5 
%Initialisation de la conséquence finale 
nbruleSF5=length(SF5.rule); %nombre de règles
nbCsqSF5=length(SF5.output.mf); %Nombres de classes de sortie
csqSF5=zeros(5,nbCsqSF5);
for i = 1:nbruleSF5,
    csqSF5(SF5.rule(i).consequent)=max(csqSF5(SF5.rule(i).consequent), declenchementSF5(i));
    
end;

%Affichage de la conséquence SF5
%Concaténation de texte 
CsqSF5Txt ='';
for i = 1:nbCsqSF5
    CsqSF5Txt=[CsqSF5Txt, sprintf('( %s|%.2f)',SF5.output.mf(i).name, csqSF5(i))];
end
CsqSF5Txt= sprintf('SF5 %s : { %s }', SF5.output.name,CsqSF5Txt(1:end));
disp(CsqSF5Txt);



%%SF11
[sortie,irr11,orr,arr]= evalfis(SF11,[Coherence, 0]);
%Initialisation de la conséquence finale 
nbruleSF11=length(SF11.rule); %nombre de règles
nbCsqSF11=length(SF11.output.mf); %Nombres de classes de sortie

for i = 1:nbruleSF11,
    
    irr11(i,2) = csqSF5(SF11.rule(i).antecedent(2));
end;

declenchementSF11 = min(irr11, [], 2);

csqSF11 = zeros(1, nbCsqSF11);
for i = 1:nbruleSF11,
    csqSF11(SF11.rule(i).consequent)=max(csqSF11(SF11.rule(i).consequent),declenchementSF11(i));  
end;

CsqSF11Txt ='';
for i = 1:nbCsqSF11
    CsqSF11Txt=[CsqSF11Txt, sprintf('( %s|%.2f)',SF11.output.mf(i).name, csqSF11(i))];
end
CsqSF11Txt= sprintf('SF11 %s : { %s }', SF11.output.name,CsqSF11Txt(1:end));
disp(CsqSF11Txt);

%%
%SF10
%Initialisation de la conséquence finale 
nbruleSF10=length(SF10.rule); %nombre de règles
nbCsqSF10=length(SF10.output.mf); %Nombres de classes de sortie

for i = 1:nbruleSF10,
    irr10(i,1) = csqSF9(SF10.rule(i).antecedent(1));
    irr10(i,2) = csqSF3(SF10.rule(i).antecedent(2));
    irr10(i,3) = csqSF4(SF10.rule(i).antecedent(3));
end;

declenchementSF10 = min(irr10, [], 2);

csqSF10 = zeros(1, nbCsqSF10);
for i = 1:nbruleSF10,
    csqSF10(SF10.rule(i).consequent)=max(csqSF10(SF10.rule(i).consequent),declenchementSF10(i));  
end

CsqSF10Txt ='';
for i = 1:nbCsqSF10
    CsqSF10Txt=[CsqSF10Txt, sprintf('( %s|%.2f)',SF10.output.mf(i).name, csqSF10(i))];
end
CsqSF10Txt= sprintf('SF10 %s : { %s }', SF10.output.name,CsqSF10Txt(1:end));
disp(CsqSF10Txt);
%% sf13 UTILISATION DE EVAL FIS

[sortie,irr13,orr,arr]= evalfis(SF13,[Referencement, Frequentation]);
%% calcul declnchement de chaque regle du SF1

declenchementSF13 = min(irr13,[],2);

%%conséquence finale du SF1 
%Initialisation de la conséquence finale 
nbruleSF13=length(SF13.rule); %nombre de règles
nbCsqSF13=length(SF13.output.mf); %Nombres de classes de sortie
csqSF13=zeros(1,nbCsqSF13);
for i = 1:nbruleSF13,
    csqSF13(SF13.rule(i).consequent)=max(csqSF13(SF13.rule(i).consequent), declenchementSF13(i));
    
end;

%Affichage de la conséquence SF13
%Concaténation de texte 
CsqSF13Txt ='';
for i = 1:nbCsqSF13
    CsqSF13Txt=[CsqSF13Txt, sprintf('( %s|%.2f)',SF13.output.mf(i).name, csqSF13(i))];
end
CsqSF13Txt= sprintf('SF13 %s : { %s }', SF13.output.name,CsqSF13Txt(1:end));
disp(CsqSF13Txt);

%% sf6 UTILISATION DE EVAL FIS
 
[sortie,irr6,orr,arr]= evalfis(SF6,[ArchitectureUsable,NbrAction]);
%% calcul declenchement de chaque regle du SF6
 
declenchementSF6 = min(irr6,[],2);
 
%%conséquence finale du SF1 
%Initialisation de la conséquence finale 
nbruleSF6=length(SF6.rule); %nombre de règles
nbCsqSF6=length(SF6.output.mf); %Nombres de classes de sortie
csqSF6=zeros(1,nbCsqSF6);
for i = 1:nbruleSF6
    csqSF6(SF6.rule(i).consequent)=max(csqSF6(SF6.rule(i).consequent), declenchementSF6(i));
    
end
 
%Affichage de la conséquence SF1
%Concaténation de texte 
CsqSF6Txt ='';
for i = 1:nbCsqSF6
    CsqSF6Txt=[CsqSF6Txt, sprintf('( %s|%.2f)',SF6.output.mf(i).name, csqSF6(i))];
end
CsqSF6Txt= sprintf('SF6 %s : { %s }', SF6.output.name,CsqSF6Txt(1:end));
disp(CsqSF6Txt);
 
%% sf7 UTILISATION DE EVAL FIS
 
[sortie,irr7,orr,arr]= evalfis(SF7,[QuoiF, CommentF, QuandF]);
%% calcul declenchement de chaque regle du SF7
 
declenchementSF7 = min(irr7,[],2);
 
%%conséquence finale du SF7 
%Initialisation de la conséquence finale 
nbruleSF7=length(SF7.rule); %nombre de règles
nbCsqSF7=length(SF7.output.mf); %Nombres de classes de sortie
csqSF7=zeros(1,nbCsqSF7);
for i = 1:nbruleSF7,
    csqSF7(SF7.rule(i).consequent)=max(csqSF7(SF7.rule(i).consequent), declenchementSF7(i));
    
end;
 
%Affichage de la conséquence SF7
%Concaténation de texte 
CsqSF7Txt ='';
for i = 1:nbCsqSF7
    CsqSF7Txt=[CsqSF7Txt, sprintf('( %s|%.2f)',SF7.output.mf(i).name, csqSF7(i))];
end
CsqSF7Txt= sprintf('SF7 %s : { %s }', SF7.output.name,CsqSF7Txt(1:end));
disp(CsqSF7Txt);

%% SF12
%Initialisation de la conséquence finale 
nbruleSF12=length(SF12.rule); %nombre de règles
nbCsqSF12=length(SF12.output.mf); %Nombres de classes de sortie

for i = 1:nbruleSF12
    irr12(i,1) = csqSF7(SF12.rule(i).antecedent(1));
    irr12(i,2) = csqSF6(SF12.rule(i).antecedent(2));
end

declenchementSF12 = min(irr12, [], 2);

csqSF12 = zeros(1, nbCsqSF12);
for i = 1:nbruleSF12
    csqSF12(SF12.rule(i).consequent)=max(csqSF12(SF12.rule(i).consequent),declenchementSF12(i));  
end

CsqSF12Txt ='';
for i = 1:nbCsqSF12
    CsqSF12Txt=[CsqSF12Txt, sprintf('( %s|%.2f)',SF12.output.mf(i).name, csqSF12(i))];
end
CsqSF12Txt= sprintf('SF12 %s : { %s }', SF12.output.name,CsqSF12Txt(1:end));
disp(CsqSF12Txt);

%%  
poids =[0.8 0.6 1.00001 1.2 1 1.000002];
conclu = [0 50 100];
note =[[(sum(csqSF8.*conclu)-sortie)*poids(1),1];... 
    [(sum(csqSF9.*conclu)-sortie)*poids(2),2];...
    [(sum(csqSF10.*conclu)-sortie)*poids(3),3];...
    [(sum(csqSF11.*conclu)-sortie)*poids(4),4];...
    [(sum(csqSF12.*conclu)-sortie)*poids(5),5];...
    [(sum(csqSF13.*conclu)-sortie)*poids(6),6]];

note2 =[(sum(csqSF8.*conclu)-sortie)*poids(1);... 
    (sum(csqSF9.*conclu)-sortie)*poids(2);...
    (sum(csqSF10.*conclu)-sortie)*poids(3);...
    (sum(csqSF11.*conclu)-sortie)*poids(4);...
    (sum(csqSF12.*conclu)-sortie)*poids(5);...
    (sum(csqSF13.*conclu)-sortie)*poids(6)];

note2 = sort(note2); % Note 2 contient les valeurs triées

for x = 1:6
    for y = 1:6
        if note2(x) == note(y,1) % Note contient les valeurs associées à leur indices,
         ind{x} = note(y,2); % cela nous permet une fois que les valeur sont triées de retrouver à quelle SF appartient chaque valeur
        end 
    end 
end

CsqFinal = (csqSF8*poids(1) + csqSF9*poids(2) + csqSF10*poids(3) + csqSF11*poids(4) + csqSF12*poids(5) + csqSF13*poids(6))/sum(poids);

conclusion = [0 50 100];

sortie = sum(CsqFinal.*conclusion)/sum(CsqFinal);

fprintf('\n\nVoici la note du site : %.2f\n', sortie);
fprintf('Voici le classement de ce qui influence négativement le plus votre note :\n')
for i = 1:6
    if note((ind{i}),1)<=0
    fprintf('%d. %s %s avec un poid de %.1f et une note de %.2f \n',i ,sprintf('SF%d',ind{i}+7),...
        eval(sprintf('SF%d',ind{i}+7)).output.name,...
        poids(ind{i}),...
        sum(eval(sprintf('csqSF%d',ind{i}+7)).*conclusion));
    end
end
