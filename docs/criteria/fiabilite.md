- fréquence d'actualisation.
- Fiabilité en cliquant un lien (https://www.virustotal.com/#/home/url).
- Protocol HTTPS.
- Politique du site.
- Propriétaire du domaine (https://www.nom-domaine.fr/whois.html).
- Detecteur de crash.
