https://www.searchbooster.fr/comment-savoir-gratuitement-si-son-site-web-est-optimise-pour-etre-bien-positionne-dans-les-resultats-naturels-de-google/

- L’adaptation aux différents supports: 
    - Mobile/Tablette : 
        - Tailles d’écrans. ( https://search.google.com/test/mobile-friendly )
        - La vitesse de chargement sur mobile. ( https://gtmetrix.com/ )
    - PC :

- Le bon remplissage des métadonnées et les données on-site (https://chrome.google.com/webstore/detail/seoquake/) :
    - L’adresse URL : www.mon-nom-de-site.com/mon-nom-de-page/
    - La balise title : le titre de ma page qui figure dans l’onglet prévu à cet effet.(70 caractères maximum)
    – Les en-têtes ou balise Hn (H1, H2, H3, H4 etc…) qui sont les titres, sous-titre et sous-sous titres des contenus de vos pages. (1 seul H1 par page et pas de saut d’une H2 à une H4 par exemple)
    – Les attributs Alt (différentes idéalement pour chaque photo) qui sont les descriptions des photos insérées dans votre site
    – La méta description qui est un résumé du contenu de votre page soit rédigé par vos soins (c’est mieux) soit généré automatiquement par Google en fonction de ce qu’il a perçu comme étant le contenu de votre page.(entre 160 et 300 caractères idéalement et différente pour chacune des pages)
    – Les mots clés métas (inutile à remplir car Google a clairement annoncé qu’il n’en tenait plus compte)

- Les données du site: (SEOQuake )
    - Un fichier robot.txt (qui indique au robot si le site peut être indexé partiellement, pas du tout ou en totalité)
    - Un fichier sitemap.xml (qui indique au robot quelle est l’arborescence de votre site et lui fait ainsi gagner du temps pour l’indexation des contenus).
    - Si la langue du site est bien spécifiée ainsi que le Doctype et l’encodage.

- fréquence d'actualisation
- Classement recherche Google
- Fréquentation : https://www.alexa.com/siteinfo/zara.com
