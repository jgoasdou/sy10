https://digitics.fr/quest-ce-que-laccessibilite-des-sites-internet/
https://www.webaccessibility.com/
https://www.powermapper.com/
https://accessibe.com/ace?website=https://www.interieur.gouv.fr/

En France, il faut se référer au WCAG 2.0, la “méthodologie unifiée d’évaluation de l’accessibilité du web”. Pour savoir si votre site internet est facilement accessible, vous devez vérifier s’il remplit les critères des WCAG 2.0, avec les niveaux A et AA. Cependant le niveau AA est obligatoire pour les sites internet publics (mairies, collectivités locales, offices de tourisme, etc).

– A : les fonctions d’accessibilité les plus élémentaires du web

– AA : traite des obstacles les plus importants et les plus courants pour les utilisateurs handicapés

– AAA : le niveau le plus élevé (et le plus complexe) d’accessibilité au web

Votre site internet n’est pas accessible ? Contactez nous !

------------------------------------------------------------------------------------

- langue disponible
- L’adaptation aux différents supports: 
    - Mobile/Tablette : 
        - tailles d’écrans : ( https://search.google.com/test/mobile-friendly ) 
    - PC :

- dispositif handicap : 
    - Sous-titres
    - Audio-description 

- Niveau WCAG 2.0
- temps de chargement minimal (GTmetrix)
